package utils

import interfaces.MuscleGroups

/**
 * Вспомогательный класс
 */
class Utils
{

    /**
     * Отображение информации по существующим группам мышц.
     */
    static String trainingInfo(List<MuscleGroups> muscleGroups)
    {
        String infoStr = ""
        muscleGroups.each { infoStr += it.groupName() + "\n" ?: "" }

        infoStr ?: ""
    }
}