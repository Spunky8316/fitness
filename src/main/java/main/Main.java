package main;

import implementation.TrainingDay;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Florinskiy Artyom
 * @date 28.03.2017
 *
 *
 * TODO создавать не дни недели а дни тренировок, где можно выбирать, в какой конкретно день недели ходить;
 * TODO создавать группы мышц самим пользователеем и вытягивать в дальнейшем из справочника.
 */

public class Main
{
    public static void main (String[] args)
    {
        //Классика
        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-classic.xml");
        TrainingDay monday = (TrainingDay) ctx.getBean("monday");

        //Аннотации
        //ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-annotate.xml");
        //Использование АОП
        //ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-aop.xml");
    }
}
