package implementation;

import interfaces.TrainingDetails;

import java.math.BigDecimal;

/**
 * @author Florinskiy Artyom
 * @date 28.03.2017
 */

public class TrainingDetail implements TrainingDetails
{
    private String approaches;
    private Integer repetitions;
    private BigDecimal weigth;
    private String breakTime;
    private Boolean isPump;
    private Boolean isSuperSet;

    public TrainingDetail(String approaches,
                          Integer repetitions,
                          BigDecimal weigth,
                          String breakTime,
                          Boolean isPump,
                          Boolean isSuperSet)
    {
        this.approaches = approaches;
        this.repetitions = repetitions;
        this.weigth = weigth;
        this.breakTime = breakTime;
        this.isPump = isPump;
        this.isSuperSet = isSuperSet;
    }

    public String approaches()
    {
        return approaches;
    }

    public Integer repetitions()
    {
        return repetitions;
    }

    public BigDecimal weight()
    {
        return weigth;
    }

    public String breakTime()
    {
        return breakTime;
    }

    public Boolean isPump()
    {
        return isPump;
    }

    public Boolean isSuperSet()
    {
        return isSuperSet;
    }
}
