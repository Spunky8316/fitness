package implementation;

import interfaces.MuscleGroups;
import interfaces.TrainingDays;
import utils.Utils;

import java.util.List;

/**
 * @author Florinskiy Artyom
 * @date 29.03.2017
 */

public class TrainingDay implements TrainingDays
{
    private String day;
    private List<MuscleGroups> muscleGroups;

    public TrainingDay(String day, List<MuscleGroups> muscleGroups)
    {
        this.day = day;
        this.muscleGroups = muscleGroups;



        System.out.println("day " + day);
        System.out.println(Utils.trainingInfo(muscleGroups));
    }

    public String day()
    {
        return day;
    }

    public List<MuscleGroups> groups()
    {
        return muscleGroups;
    }

    public void setDay(String day)
    {
        this.day = day;
    }

}
