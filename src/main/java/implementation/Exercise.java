package implementation;

import interfaces.Exercises;

/**
 * @author Florinskiy Artyom
 * @date 28.03.2017
 */

public class Exercise implements Exercises
{
    private String exerciseName;
    private String description;

    public Exercise(String exerciseName, String description)
    {
        this.exerciseName = exerciseName;
        this.description = description;
    }


    public String exerciseName()
    {
        return exerciseName;
    }

    public String description()
    {
        return description;
    }
}
