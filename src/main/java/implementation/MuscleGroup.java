package implementation;

import interfaces.Exercises;
import interfaces.MuscleGroups;
import interfaces.TrainingDetails;

import java.util.List;

/**
 * @author Florinskiy Artyom
 * @date 29.03.2017
 */

public class MuscleGroup implements MuscleGroups
{
    private String groupName;
    private List<Exercises> exercises;
    private TrainingDetails trainingDetails;

    public MuscleGroup(String groupName,
                       List<Exercises> exercises,
                       TrainingDetails trainingDetails)
    {
        this.groupName = groupName;
        this.exercises = exercises;
        this.trainingDetails = trainingDetails;
    }

    public String groupName()
    {
        return groupName;
    }

    public List<Exercises> exercises()
    {
        return exercises;
    }

    public TrainingDetails trainingDetails()
    {
        return trainingDetails;
    }
}
