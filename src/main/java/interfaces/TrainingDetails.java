package interfaces;

import java.math.BigDecimal;

/**
 * @author Florinskiy Artyom
 * @date 28.03.2017
 */

public interface TrainingDetails
{
    //Кол-во повторений
    String approaches();

    //Кол-во подходов
    Integer repetitions();

    //Вес
    BigDecimal weight();

    //Перерывы между подходами
    String breakTime();

    Boolean isPump();
    Boolean isSuperSet();
}
