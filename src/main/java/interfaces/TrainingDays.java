package interfaces;

import java.util.List;

/**
 * @author Florinskiy Artyom
 * @date 28.03.2017
 */

public interface TrainingDays
{
    String day();
    List<MuscleGroups> groups();
}
