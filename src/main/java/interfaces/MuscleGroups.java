package interfaces;

import java.util.List;

/**
 * @author Florinskiy Artyom
 * @date 28.03.2017
 */

public interface MuscleGroups
{
    String groupName();
    List<Exercises> exercises();
    TrainingDetails trainingDetails();
}
