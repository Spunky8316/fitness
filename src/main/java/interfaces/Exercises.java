package interfaces;

/**
 * @author Florinskiy Artyom
 * @date 28.03.2017
 */

public interface Exercises
{
    String exerciseName();
    String description();
}
